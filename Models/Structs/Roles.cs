﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntenzNews.Models.Structs
{
    public struct Roles
    {
        public const string User = "User";
        public const string Admin = "Admin";
    }
}
