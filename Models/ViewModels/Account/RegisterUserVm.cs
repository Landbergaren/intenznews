﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IntenzNews.Models.ViewModels
{
    public class RegisterUserVm
    {
        [Required]
        [MinLength(3, ErrorMessage = "You need at least 3 characters")]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required, Compare(nameof(Password)), Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
        [Required, EmailAddress, MinLength(6)]
        public string Email { get; set; }
        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; } = false;
    }
}
