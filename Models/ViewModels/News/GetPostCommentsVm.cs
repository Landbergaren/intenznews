﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntenzNews.Models.ViewModels
{
    public class GetPostCommentsVm
    {
        public PostCommentVm[] CommentsForPost { get; set; }
        public bool NoMoreNews { get; set; }
    }
}
