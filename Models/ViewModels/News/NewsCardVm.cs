﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntenzNews.Models.ViewModels
{
    public class NewsCardVm
    {
        public int Id { get; set; }
        public string Author { get; set; } 
        public string Header { get; set; }
        public DateTime PublishedAt { get; set; }
        public string Content { get; set; }
        public int LikesCount { get; set; }
        public int CommentCount { get; set; }
        public bool Liked { get; set; }
    }
}
