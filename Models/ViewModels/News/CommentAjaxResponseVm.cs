﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntenzNews.Models.ViewModels
{
    public class CommentAjaxResponseVm
    {
        public int CommentCount { get; set; }
        public bool PostedSuccessfully { get; set; }
        public PostCommentVm NewComment { get; set; }
    }
}
