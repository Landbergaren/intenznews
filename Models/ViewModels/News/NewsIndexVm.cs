﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntenzNews.Models.ViewModels
{
    public class NewsIndexVm
    {
        public bool HasNews { get; set; } = false;
        public List<NewsCardVm> NewsList { get; set; }  
        public CreatePostVm CreatePostVm { get; set; }
    }

    [Bind(Prefix = nameof(NewsIndexVm.CreatePostVm))]
    public class CreatePostVm
    {
        public string Header { get; set; }
        public string Content { get; set; }
    }
}
