﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntenzNews.Models.ViewModels
{
    public class PostCommentVm
    {
        public string Author { get; set; }
        public string Content { get; set; }
        public DateTime PublishedAt { get; set; }
        public int Page { get; set; }
    }
}
