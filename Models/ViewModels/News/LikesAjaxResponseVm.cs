﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntenzNews.Models.ViewModels
{
    public class LikesAjaxResponseVm
    {
        public int AmountOfLikes { get; set; }
    }
}
