using AutoMapper;
using IntenzNews.Models.Entities;
using IntenzNews.Models.Structs;
using IntenzNews.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace IntenzNews.Models.services
{
    public class AccountServices
    {
        readonly IMapper mapper;
        readonly IzNewsIdentityContext _identityCtx;
        readonly UserManager<IdentityUser> _userManager;
        readonly SignInManager<IdentityUser> _signInManager;
        readonly RoleManager<IdentityRole> _roleManager;

        public AccountServices(
            IMapper map,
            IzNewsIdentityContext identityCtx,
            UserManager<IdentityUser> userMan,
            SignInManager<IdentityUser> signInMan,
            RoleManager<IdentityRole> roleMan    
            )
        {
            _identityCtx = identityCtx;
            _userManager = userMan;
            mapper = map;
            _signInManager = signInMan;
            _roleManager = roleMan;
        }

        public void CreateIdentityDb() {
            _identityCtx.Database.EnsureCreated();
        }

        public async Task<IdentityResult> CreateUserAsync(RegisterUserVm userVm)
        {
            var newUser = mapper.Map<IdentityUser>(userVm);
            var createResult = await _userManager.CreateAsync(newUser, userVm.Password);
            if (!createResult.Succeeded)
            return createResult;

            var roleResult = await _userManager.AddToRoleAsync(newUser, Roles.User);
            return roleResult;
        }

        public async Task<IdentityResult> CreateTestAdmin()
        {
            var newAdmin = new IdentityUser { UserName = "Tomas" };
            var createResult = await _userManager.CreateAsync(newAdmin, "Password");
            if (!createResult.Succeeded)
                return createResult;
            var roleResult = await _userManager.AddToRoleAsync(newAdmin, Roles.Admin);
            return roleResult;
        }

        internal async Task LogoutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<SignInResult> LoginAsync(LoginVm loginVm)
        {
            var signInResult = await _signInManager.PasswordSignInAsync(loginVm.UserName, loginVm.Password, loginVm.RememberMe, false);
            return signInResult;
        }

        public async Task CreateRolesIfNotExistAsync()
        {
            if (!await _roleManager.RoleExistsAsync(Roles.Admin) && !await _roleManager.RoleExistsAsync(Roles.User))
            {
                await _roleManager.CreateAsync(new IdentityRole { Name = Roles.User });
                await _roleManager.CreateAsync(new IdentityRole { Name = Roles.Admin });
            }

        }
    }
}