using AutoMapper;
using IntenzNews.Models.Entities;
using IntenzNews.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace IntenzNews.Models.services
{
    public class NewsServices
    {
        private readonly IMapper mapper;
        private readonly IzNewsContext newsCtx;
        private readonly HtmlEncoder htmlEncoder;
        private readonly UserManager<IdentityUser> userManager;

        public NewsServices(IMapper mapper, IzNewsContext newsCtx, HtmlEncoder htmlEncoder, UserManager<IdentityUser> userManager)
        {
            this.mapper = mapper;
            this.newsCtx = newsCtx;
            this.htmlEncoder = htmlEncoder;
            this.userManager = userManager;
        }

        public async Task<List<NewsCardVm>> GetNewsAsync(ClaimsPrincipal currentUser)
        {
            var userId = userManager.GetUserId(currentUser);
            var newsList = await newsCtx.NewsPost.Include(p => p.Author).Include(p => p.Comment).Include(p => p.Like).Select(p => mapper.Map<NewsCardVm>(p)).ToListAsync();
            var orderedList = newsList.OrderByDescending(p => p.PublishedAt).ToList();
            var likedPosts = newsCtx.Like.Where(l => l.UserId == userId).Select( i => i.PostId ).ToList();
            foreach (var likedId in likedPosts)
            {
                foreach (var newsVm in orderedList)
                {
                    if (!newsVm.Liked)
                        newsVm.Liked = newsVm.Id == likedId;
                }
            }
            return orderedList;
        }

        public async Task<bool> CreatePostAsync(CreatePostVm newPost, ClaimsPrincipal currentUser)
        {
            var userId = userManager.GetUserId(currentUser);
            await newsCtx.NewsPost.AddAsync(new NewsPost { AuthorId = userId, Content = newPost.Content, Header = newPost.Header, PublishedAt = DateTime.Now, });
            try
            {
                await newsCtx.SaveChangesAsync();
            }
            catch (Exception)
            {
                return false;
                throw;
            }
            return true;
        }

        internal async Task LikePostAsync(int postId, ClaimsPrincipal currentUser)
        {
            var userId = userManager.GetUserId(currentUser);
            var like = await newsCtx.Like.Where(p => (p.UserId == userId) && (p.PostId == postId)).FirstOrDefaultAsync();
            if (like == null)
                await newsCtx.Like.AddAsync(new Like { PostId = postId, UserId = userId });            
            else
                newsCtx.Like.Remove(like);

            await newsCtx.SaveChangesAsync();            
        }


        internal async Task DeletePostAsync(int postId)
        {
            //var entitiesToDelete = newsCtx.NewsPost.Where(n => n.Id == postId ).Include( n => n.Comment).Include( n => n.Like).ToListAsync();
            var newsToDelete = newsCtx.NewsPost.Where(n => n.Id == postId);
            var commentsToDelete = newsCtx.Comment.Where(c => c.PostId == postId);
            var likesToDelete = newsCtx.Like.Where(l => l.PostId == postId);
            newsCtx.RemoveRange(commentsToDelete);
            newsCtx.RemoveRange(likesToDelete);
            newsCtx.RemoveRange(newsToDelete);
            await newsCtx.SaveChangesAsync();
        }

        internal async Task<int> GetLikeCountForPostAsync(int postId)
        {
            var totalLikesAmount = await newsCtx.Like.Where(p => p.PostId == postId).CountAsync();
            return totalLikesAmount;
        }

        internal async Task<PostCommentVm> CommentPostAsync(int postId, string comment, ClaimsPrincipal currentUser)
        {
            var userId = userManager.GetUserId(currentUser);
            var encodedComment = htmlEncoder.Encode(comment);
            var newComment = new Comment { AuthorId = userId, Content = encodedComment, PostId = postId, PublishedAt = DateTime.Now };
            try
            {
                await newsCtx.Comment.AddAsync(newComment);
                await newsCtx.SaveChangesAsync();                
            }
            catch (Exception)
            {
                return null;
            }
            var commentVm = mapper.Map<PostCommentVm>(newComment);
            return commentVm;
        }

        internal async Task<int> GetCommentCountForPostAsync(int postId)
        {
            var commentCount = await newsCtx.Comment.Where(c => c.PostId == postId).CountAsync();
            return commentCount;
        }

        
        internal async Task<PostCommentVm[]> GetNextPageOfCommentsAsync(int postId, int page)
        {
            const int COMMENT_COUNT = 5;
            var skipCount = --page * COMMENT_COUNT;
            var postComments = await newsCtx.Comment
                .Include(p => p.Author)
                .Where(c => c.PostId == postId)
                .Select( p => mapper.Map<PostCommentVm>(p))
                .OrderByDescending(p => p.PublishedAt)
                .Skip(skipCount)
                .Take(COMMENT_COUNT)
                .ToArrayAsync();
            return postComments;
        }
    }
}