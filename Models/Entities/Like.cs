﻿using System;
using System.Collections.Generic;

namespace IntenzNews.Models.Entities
{
    public partial class Like
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int PostId { get; set; }

        public virtual NewsPost Post { get; set; }
        public virtual AspNetUsers User { get; set; }
    }
}
