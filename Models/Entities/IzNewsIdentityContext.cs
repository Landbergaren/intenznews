﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntenzNews.Models.Entities
{
    public class IzNewsIdentityContext : IdentityDbContext<IdentityUser>
    {
        public IzNewsIdentityContext(DbContextOptions<IzNewsIdentityContext> options) : base (options)
        {

        }
    }
}
