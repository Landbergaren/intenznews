﻿using System;
using System.Collections.Generic;

namespace IntenzNews.Models.Entities
{
    public partial class AspNetUsers
    {
        public AspNetUsers()
        {
            Comment = new HashSet<Comment>();
            Like = new HashSet<Like>();
            NewsPost = new HashSet<NewsPost>();
        }

        public string Id { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }

        public virtual ICollection<Comment> Comment { get; set; }
        public virtual ICollection<Like> Like { get; set; }
        public virtual ICollection<NewsPost> NewsPost { get; set; }
    }
}
