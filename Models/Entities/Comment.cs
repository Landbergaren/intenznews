﻿using System;
using System.Collections.Generic;

namespace IntenzNews.Models.Entities
{
    public partial class Comment
    {
        public int Id { get; set; }
        public string AuthorId { get; set; }
        public int PostId { get; set; }
        public string Content { get; set; }
        public DateTime PublishedAt { get; set; }

        public virtual AspNetUsers Author { get; set; }
        public virtual NewsPost Post { get; set; }
    }
}
