﻿using System;
using System.Collections.Generic;

namespace IntenzNews.Models.Entities
{
    public partial class NewsPost
    {
        public NewsPost()
        {
            Comment = new HashSet<Comment>();
            Like = new HashSet<Like>();
        }

        public int Id { get; set; }
        public string AuthorId { get; set; }
        public string Header { get; set; }
        public string Content { get; set; }
        public DateTime PublishedAt { get; set; }

        public virtual AspNetUsers Author { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
        public virtual ICollection<Like> Like { get; set; }
    }
}
