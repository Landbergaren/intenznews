CREATE TABLE [Iz].[NewsPost] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [AuthorId]    NVARCHAR (450)  NULL,
    [Header]      NVARCHAR (100)  NOT NULL,
    [Content]     NVARCHAR (1000) NOT NULL,
    [PublishedAt] DATETIME        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_NewsPost_ToUser] FOREIGN KEY ([AuthorId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);

