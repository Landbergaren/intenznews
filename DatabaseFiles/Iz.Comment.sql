CREATE TABLE [Iz].[Comment] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [AuthorId]    NVARCHAR (450) NULL,
    [PostId]      INT            NOT NULL,
    [Content]     NVARCHAR (800) NOT NULL,
    [PublishedAt] DATETIME       NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Comment_ToUser] FOREIGN KEY ([AuthorId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_ToPostTable] FOREIGN KEY ([PostId]) REFERENCES [Iz].[NewsPost] ([Id])
);

