CREATE TABLE [Iz].[Like] (
    [Id]     INT            IDENTITY (1, 1) NOT NULL,
    [UserId] NVARCHAR (450) NULL,
    [PostId] INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_LikedPost] FOREIGN KEY ([PostId]) REFERENCES [Iz].[NewsPost] ([Id]),
    CONSTRAINT [FK_Like_ToUser] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);

