﻿$(document).ready(function () {
    let news = [];
    let newsCards = $(".news-card");
    //build news "state"
    for (let i = 0; i < newsCards.length; i++) {
        news.push({
            postId: $(newsCards[i]).attr("data-post"),
            page: 0,
            comments: []
        });
    }

    // remove uglyness in js default date format
    let formatDate = function (date) {
        formatedDate = new Date(date);
        return formatedDate.toLocaleString();
    };

    // adds comment as last element to comment section
    let appendComment = function (postId, author, publishedAt, content) {
        $(`#card-id-${postId} .comments`).append(`<div class="comment-div"></div>`);
        $(`#card-id-${postId} .comments`).children(":last")
            .append(`<div><span class="comment-author">${author}</span><small class="comment-date">${formatDate(publishedAt)}</small></div>`)
            .append("<hr />")
            .append(`<div class="comment-content">${content}</div>`);
    };

    // adds comment as first element to comment section
    let prependComment = function (postId, author, publishedAt, content) {
        $(`#card-id-${postId} .comments`).prepend(`<div class="comment-div"></div>`);
        $(`#card-id-${postId} .comments`).children(":first")
            .append(`<div><span class="comment-author">${author}</span><small class="comment-date">${formatDate(publishedAt)}</small></div>`)
            .append("<hr />")
            .append(`<div class="comment-content">${content}</div>`);
    };

    // returns next pagenumber for specified post
    let getPageForPost = function (postId) {
        let nextPage;
        for (var post of news) {
            if (post.postId === postId) {
                nextPage = post.page + 1;
                return nextPage;
            }
        }
        return false;
    };

    // increments page-tracking by changing 
    let incrementPageCount = function (postId) {
        for (var post of news) {
            if (post.postId === postId) {
                post.page++;
            }
        }
    };

    // like/unlinke news with a request to backend
    $(".news-like").click(function () {
        let postId = $(this).parent().attr("data-post");
        let viewmodel = {
            postId: postId
        };
        $.ajax({
            url: "news/like",
            method: "POST",
            data: viewmodel
        }).done(result => {
            $(`#card-id-${postId} .like-count`).text(result.amountOfLikes);
            $(`#card-id-${postId} .fa-heart`).toggleClass("liked");
        });
    });

    // get older posts (5) from backend
    $(".older-posts").click(function () {
        let postId = $(this).parents(".news-card").attr("data-post");
        let nextPage = getPageForPost(postId);
        $.get(`News/GetNextPageOfComments/${postId}/${nextPage}`).done(result => {
            if (result.noMoreNews)
                $(`#card-id-${postId} .older-posts`).hide();
            for (var comment of result.commentsForPost) {

                appendComment(postId, comment.author, comment.publishedAt, comment.content);
            }
            incrementPageCount(postId);
        });
    });

    // Show comments, get first 5 comments, mark button as clicked in html attribute
    $(".show-comments").click(function () {
        let clicked = $(this).attr("data-clicked");
        $(this).toggleClass("fa-angle-down").toggleClass("fa-angle-up");
        if (clicked === "true") {
            return;
        }
        let postId = $(this).parents(".news-card").attr("data-post");
        $(this).attr("data-clicked", "true");
        let nextPage = getPageForPost(postId);
        $.get(`News/GetNextPageOfComments/${postId}/${nextPage}`).done(result => {
            if (result.commentsForPost.length > 0)
                $(`#card-id-${postId} .comments`).show();
            if (result.noMoreNews) {
                $(`#card-id-${postId} .older-posts`).hide();
            }
            for (var comment of result.commentsForPost) {
                formatDate(comment.publishedAt);
                appendComment(postId, comment.author, comment.publishedAt, comment.content);
            }
            incrementPageCount(postId);
        });
    });

    // Post comment to server and appends to comment section if successfull
    $(`.post-comment`).click(function () {
        let postId = $(this).parents(".news-card").attr("data-post");
        let commentToPost = $(`#card-id-${postId} .comment-input`).val();
        let viewModel = { comment: commentToPost, postId: postId };
        $.ajax({
            url: "News/PostComment",
            method: "POST",
            data: viewModel
        }).done(result => {
            $(`#post-comment-${postId} .comment-input`).val("");
            $(`#card-id-${postId} .comments`).show();
            comment = result.newComment;
            prependComment(postId, comment.author, comment.publishedAt, comment.content);
            $(`#card-id-${postId} .comment-count`).text(result.commentCount);
        });
    });

    // Delete posts with request and removes from view if successfull
    $(".remove-news").click(function () {
        let postId = $(this).parents(".news-card").attr("data-post");
        $.ajax({
            url: "News/DeletePost/" + postId,
            method: "GET",
            success: result => {
                console.log(result);
                $(`#card-id-${postId}`).parents(".news-post").remove();
            },
            error: error => {
                console.error("Could not return a correct response");
            }
        });
    });
});