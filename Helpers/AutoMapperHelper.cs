﻿using AutoMapper;
using IntenzNews.Models.Entities;
using IntenzNews.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace IntenzNews.Helpers
{
    public class AutoMapperHelper : Profile
    {
        public AutoMapperHelper()
        {
            CreateMap<RegisterUserVm, IdentityUser>();
            CreateMap<NewsCardVm, NewsPost>();
            CreateMap<PostCommentVm, Comment>();
            CreateMap<Comment, PostCommentVm>()
                .ForMember(dest => dest.Author, opt =>
                {
                    opt.MapFrom(d => d.Author.UserName);
                });
            CreateMap<NewsPost, NewsCardVm>()
            .ForMember(dest => dest.LikesCount, opt =>
             {
                 opt.MapFrom(d => d.Like.Count);
             })
            .ForMember(dest => dest.CommentCount, opt =>
             {
                 opt.MapFrom(d => d.Comment.Count);
             })
            .ForMember(dest => dest.Author, opt =>
            {
                opt.MapFrom(d => d.Author.UserName);
            });
        }
    }
}
