﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using IntenzNews.Models.services;
using IntenzNews.Models.Structs;
using IntenzNews.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IntenzNews.Controllers
{
    [Authorize]
    public class NewsController : Controller
    {
        private readonly NewsServices newsServices;

        public NewsController(NewsServices newsServices)
        {
            this.newsServices = newsServices;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index(NewsIndexVm newsVm)
        {
            if (!String.IsNullOrEmpty((string)TempData["FailedToCreate"]))
                ModelState.AddModelError("CreatePostVm.Content", TempData["FailedToCreate"].ToString());
            newsVm.NewsList = await newsServices.GetNewsAsync(HttpContext.User);
            newsVm.HasNews = newsVm.NewsList.Count > 0;
            return View(newsVm);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePost(CreatePostVm newsVm)
        {
            if (!ModelState.IsValid)
                return View(newsVm);

            var createSucceeded = await newsServices.CreatePostAsync(newsVm, HttpContext.User);
            if (!createSucceeded)
            {
                string s = "Post could not be created";
                TempData["FailedToCreate"] = s;
                return RedirectToAction(nameof(Index));
            }                
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Like(LikedPostVm postVm)
        {
            await newsServices.LikePostAsync(postVm.PostId, HttpContext.User);
            var amountOfLikes = await newsServices.GetLikeCountForPostAsync(postVm.PostId);
            return Json(new LikesAjaxResponseVm { AmountOfLikes = amountOfLikes });
        }

        [HttpPost]        
        public async Task<IActionResult> PostComment(PostNewCommentVm commentVm)
        {
            var currentUser = HttpContext.User;
            var newComment = await newsServices.CommentPostAsync(commentVm.postId, commentVm.Comment, currentUser);
            if (newComment == null)
                return Json(new CommentAjaxResponseVm { PostedSuccessfully = false });
           
            var amountOfComments = await newsServices.GetCommentCountForPostAsync(commentVm.postId);
            return Json( new CommentAjaxResponseVm { CommentCount = amountOfComments, PostedSuccessfully = newComment != null, NewComment = newComment });
        }

        [HttpGet, Authorize(Roles = Roles.Admin), Route("News/" +nameof(DeletePost) + "/{postId}")]
        public async Task<IActionResult>DeletePost(int postId)
        {
            await newsServices.DeletePostAsync(postId);
            return RedirectToAction(nameof(Index));
        }

        [AllowAnonymous, HttpGet]
        [Route("News/" + nameof(GetNextPageOfComments) + "/{postId}/{page}")]
        public async Task<IActionResult> GetNextPageOfComments (int postId, int page)
        {
            var comments = await newsServices.GetNextPageOfCommentsAsync(postId, page);
            return Json(new GetPostCommentsVm { CommentsForPost = comments, NoMoreNews = comments.Count() < 5 });
        }
    }
}