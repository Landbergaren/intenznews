using IntenzNews.Models.services;
using IntenzNews.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace IntenzNews.Controllers
{
    public class HomeController : Controller
    {
        readonly AccountServices _accService;
        public HomeController(AccountServices accSer)
        {
            _accService = accSer;
        }

        public IActionResult Index() 
        {
            return View();
        }
    }
}