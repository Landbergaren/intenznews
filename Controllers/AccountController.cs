﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntenzNews.Models.services;
using IntenzNews.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace IntenzNews.Controllers
{
    public class AccountController : Controller
    {
        AccountServices _accService;

        public AccountController(AccountServices accServ)
        {
            _accService = accServ;
        }

        public async Task<IActionResult> Login(string ReturnUrl = "")
        {
            //var result = await _accService.CreateTestAdmin();
            await _accService.CreateRolesIfNotExistAsync();
            return View(new LoginVm { ReturnUrl = ReturnUrl });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginVm loginVm)
        {
            if (!ModelState.IsValid)
                return View(loginVm);

            var loginResult = await _accService.LoginAsync(loginVm);
            if (!loginResult.Succeeded)
            {
                ModelState.AddModelError(String.Empty, "Login Failed");
                return View(loginVm);
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Logout()
        {
            await _accService.LogoutAsync();
            return RedirectToAction("index", "home");
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(RegisterUserVm userVm)
        {
            if (!ModelState.IsValid)
                return View(userVm);

            var createResult = await _accService.CreateUserAsync(userVm);
            if (!createResult.Succeeded)
            {
                foreach (var error in createResult.Errors)
                {
                    ModelState.AddModelError(String.Empty, error.Description);
                }
                return View(userVm);
            }
            //success
            return RedirectToAction("Index", "News");
        }
    }
}